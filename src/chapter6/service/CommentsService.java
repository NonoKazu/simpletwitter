package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Comments;
import chapter6.dao.CommentsDao;

public class CommentsService {

    public void insert(Comments comments) {

        Connection connection = null;
        try {
            connection = getConnection();
            new CommentsDao().insert(connection, comments);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<Comments> select() {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();

            List<Comments> comments = new CommentsDao().select(connection,LIMIT_NUM);
            commit(connection);

            return comments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}