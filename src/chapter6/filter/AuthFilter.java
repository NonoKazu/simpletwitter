package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns={"/setting","/edit"})
public class AuthFilter implements Filter{
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain){
    try{
      String target = ((HttpServletRequest)request).getRequestURI();

      HttpSession session = ((HttpServletRequest)request).getSession();

      if (session == null){
        /* まだ認証されていない */
        session = ((HttpServletRequest)request).getSession(true);
        session.setAttribute("target", target);
        List<String> errorMessages = new ArrayList<String>();
        errorMessages.add("ログインしてください");
        request.setAttribute("errorMessages", errorMessages);
        request.getRequestDispatcher("/login").forward(request, response);
        return;
      }else{
        Object loginCheck = session.getAttribute("loginUser");
        if (loginCheck == null){
          /* まだ認証されていない */
          session.setAttribute("target", target);
          List<String> errorMessages = new ArrayList<String>();
          errorMessages.add("ログインしてください");
          request.setAttribute("errorMessages", errorMessages);
          request.getRequestDispatcher("/login").forward(request, response);
          return;
        }
      }

      chain.doFilter(request, response);
    }catch (ServletException se){
    }catch (IOException e){
    }
  }

  public void init(FilterConfig filterConfig) throws ServletException{
  }

  public void destroy(){
  }
}