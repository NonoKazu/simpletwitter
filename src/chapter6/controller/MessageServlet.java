 package chapter6.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comments;
import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.beans.UserMessage;
import chapter6.service.CommentsService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");
        if (!isValid(text, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        Message message = new Message();
        message.setText(text);

        User user = (User) session.getAttribute("loginUser");
        message.setUserId(user.getId());

        new MessageService().insert(message);
        response.sendRedirect("./");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException{
    	boolean isShowMessageForm = false;
		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		Date startDate=null;
		Date endDate=null;

		if(!start.isEmpty() && !end.isEmpty()) {
			startDate = Date.valueOf(start);
			endDate = Date.valueOf(end);

		}else if(!start.isEmpty()) {
			startDate = Date.valueOf(start);
		}else if(!end.isEmpty()) {
			endDate = Date.valueOf(end);
		}else {
			response.sendRedirect("./");
			return;
		}
		List<UserMessage> messages = new MessageService().select(startDate,endDate);
		request.setAttribute("messages", messages);
		request.setAttribute("start", start);
		request.setAttribute("end", end);
		List<Comments> comments = new CommentsService().select();
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("/top.jsp").forward(request, response);

    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}