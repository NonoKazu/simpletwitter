 package chapter6.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class MessageEditServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();
        HttpSession session = request.getSession();

        String text = request.getParameter("text");
		int id = Integer.parseInt(request.getParameter("id"));
        Message message = new MessageService().select(id);

        if(message == null) {
        	errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
        }

        if (!isValid(text, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
        }

        message.setText(text);

        new MessageService().update(message);
        response.sendRedirect("./");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

    	request.setCharacterEncoding("UTF-8");
    	HttpSession session = request.getSession();

    	 List<String> errorMessages = new ArrayList<String>();


    	String str =  URLEncoder.encode(request.getParameter("id"),"UTF-8");
    	if(!str.matches("^[0-9]+$")) {
    		errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
    	}
		int id = Integer.parseInt(request.getParameter("id"));
        Message message = new MessageService().select(id);

        if(message == null) {
        	errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
        }

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}
    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}