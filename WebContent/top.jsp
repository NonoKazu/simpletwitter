<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>簡易Twitter</title>

<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet">

<script type="text/javascript" src="./js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="./js/check.js"></script>
<script type="text/javascript" src="./js/action.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>

</head>

<body>
	<div class="main-contents">
		<div class="back">

		<!-- ページ上部 -->
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>


		<!-- ログインユーザの情報表示 -->
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>


		<!-- エラーメッセージの表示 -->
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


		<!-- つぶやきフォーム -->
		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br /> <input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>


		<!-- つぶやき絞り込み -->
		<div id="refinement">
			<form action="message" method="get">

				<p>
					開始日: <input name="start" type="text" id="input1" value="${start}" /> ～ 終了日: <input name="end" type="text" id="input2" value="${end}" />
				</p>
				<script>
					$(function() {
						$.datepicker.setDefaults($.datepicker.regional["ja"]);
						$("#input1").datepicker({
							altFormat : 'yy-mm-dd',
							altField : '[name="start"]'
						});
					});
					$(function() {
						$.datepicker.setDefaults($.datepicker.regional["ja"]);
						$("#input2").datepicker({
							altFormat : 'yy-mm-dd',
							altField : '[name="end"]'
						});
					});
				</script>
				<input type="submit" value="絞り込み">
			</form>
			<hr color="#ff6c94">
		</div>


		<!-- つぶやき表示部 -->
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account"> <a
							href="./?user_id=<c:out value="${message.userId}"/> "> <c:out
									value="${message.account}" />
						</a>
						</span><span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="text"><c:out value="${message.text}" /></div>
					<c:if test="${message.userId==loginUser.id}">
						<div style="display: inline-flex">
							<form action="delete" method="get" onSubmit='return check()'>
								<input name="message" value="${message.id}" type="hidden" /> <input
									type="image" src="./image/trash.png" name="削除ボタン" onClick='return jumpConfirm(this);' class="button">
							</form>
							<form action="edit" method="get">
								<input name="id" value="${message.id}" type="hidden" /><input
									type="image" src="./image/pen.png" name="編集ボタン" class="button">
							</form>
						</div>
					</c:if>
					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id==comment.messageId}">
								<div class="text"><c:out value="${comment.text}" /></div>
							</c:if>
						</c:forEach>
					</div>
					<p class="commentShow"><u>コメント表示/非表示</u></p>

				</div>
				<c:if test="${ isShowMessageForm }">
					<form action="comments" method="post">
						<textarea name="text" cols="100" rows="1" class="comments-box"></textarea>
						<input name="message" value="${message.id}" type="hidden" /> <br />
						<input type="submit" value="返信">（140文字まで）
					</form>
				</c:if>
				<hr color="#ff6c94">
			</c:forEach>

		</div>
		<div class="copyright">Copyright(c)KazukiNonoshita</div>
		</div>
	</div>
</body>
</html>